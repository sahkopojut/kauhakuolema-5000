var searchData=
[
  ['taskleg_5fback',['TaskLeg_Back',['../kauhis__mega_8ino.html#a6c745c1721bcab60b172b8ea811bdabf',1,'kauhis_mega.ino']]],
  ['taskleg_5ffront',['TaskLeg_Front',['../kauhis__mega_8ino.html#a6a0b8349ccf4e65e9014688a1c23f912',1,'kauhis_mega.ino']]],
  ['taskleg_5fmid',['TaskLeg_Mid',['../kauhis__mega_8ino.html#a3b29a6853aaf8124c93259e22403a509',1,'kauhis_mega.ino']]],
  ['taskrxtx',['TaskRXTX',['../kauhis__mega_8ino.html#a236da352796ec3b51a4d97ff043435a2',1,'kauhis_mega.ino']]],
  ['tip',['tip',['../structlegMessage.html#a41521498979662f5ab98d450fe81293e',1,'legMessage']]],
  ['tip_5fby_5fextension',['tip_by_extension',['../classleg.html#a1d9d2e3da3c7223c3032d4038288791a',1,'leg']]],
  ['txbuf',['TXBuf',['../serialForMega_8ino.html#a8852c7c207c8ff9656390cee13bf3f16',1,'serialForMega.ino']]],
  ['txbuffer',['TXBuffer',['../serialForMega_8ino.html#a9f51ae8d5253eb6f81193bc3562b6e4c',1,'TXBuffer():&#160;serialForMega.ino'],['../SerialStuff_8cpp.html#a9f51ae8d5253eb6f81193bc3562b6e4c',1,'TXBuffer():&#160;serialForMega.ino']]],
  ['txbufpop',['TXBufpop',['../SerialStuff_8cpp.html#a3185ad40a557c7487dfe12bb42a9f3b4',1,'SerialStuff.cpp']]],
  ['txbufpush',['TXBufpush',['../SerialStuff_8cpp.html#a82e63afa24cf689fa10375e58fb78cfa',1,'SerialStuff.cpp']]],
  ['txbufwrite',['TXBufWrite',['../SerialStuff_8cpp.html#a66f3228a4e9127db208a6514aabcc43f',1,'SerialStuff.cpp']]],
  ['txen',['TXEn',['../SerialStuff_8cpp.html#a71bf7108b13725cdd7dda6ec4c12f6ab',1,'SerialStuff.cpp']]],
  ['txin',['TXIn',['../serialForMega_8ino.html#a381e4c593a142c7281959a0d6472e0da',1,'TXIn():&#160;serialForMega.ino'],['../SerialStuff_8cpp.html#a381e4c593a142c7281959a0d6472e0da',1,'TXIn():&#160;serialForMega.ino']]],
  ['txout',['TXOut',['../serialForMega_8ino.html#a4a477cb761535c4d39fdd41d26a0b002',1,'TXOut():&#160;serialForMega.ino'],['../SerialStuff_8cpp.html#a4a477cb761535c4d39fdd41d26a0b002',1,'TXOut():&#160;serialForMega.ino']]]
];

var searchData=
[
  ['last_5fserial_5fcommand_5fsent',['last_serial_command_sent',['../bleToSerial__2_8ino.html#a0b1a1d96131158288c2f5e9f3977b7e0',1,'bleToSerial_2.ino']]],
  ['lastserialreceive',['lastSerialReceive',['../group__mega2560.html#ga79c351f6555f0ef26c103a830e8bb6fe',1,'main.ino']]],
  ['leg',['leg',['../classleg.html',1,'leg'],['../classleg.html#a4d20b7f832acabf6216dcb6fb7ecc8c2',1,'leg::leg(int nTip_len, int nMid_len, int nRest_height, int nWalk_clearance)'],['../classleg.html#a418877dece641560d83d01fa236ec8a4',1,'leg::leg()']]],
  ['legmessage',['legMessage',['../structlegMessage.html',1,'']]],
  ['legs',['legs',['../bleToSerial__2_8ino.html#a7c8536aea5c3277b376ff7545400d347',1,'bleToSerial_2.ino']]],
  ['legs_2ehpp',['legs.hpp',['../legs_8hpp.html',1,'']]],
  ['legs_2eino',['legs.ino',['../legs_8ino.html',1,'']]],
  ['log',['log',['../group__mega2560.html#ga86e09be2bbc3f5a3b9f916f910c645b4',1,'log(T loggable):&#160;legs.ino'],['../legs_8ino.html#a86e09be2bbc3f5a3b9f916f910c645b4',1,'log(T loggable):&#160;legs.ino'],['../group__mega2560.html#ga86e09be2bbc3f5a3b9f916f910c645b4',1,'log(T loggable):&#160;main.ino']]],
  ['logging',['logging',['../group__mega2560.html#ga955fc0128b4baa82a155b16d6fb5729d',1,'main.ino']]],
  ['logln',['logln',['../group__mega2560.html#ga380cd75a227b957715da0e24ef3e1f46',1,'logln(T loggable):&#160;legs.ino'],['../legs_8ino.html#a380cd75a227b957715da0e24ef3e1f46',1,'logln(T loggable):&#160;legs.ino'],['../group__mega2560.html#ga380cd75a227b957715da0e24ef3e1f46',1,'logln(T loggable):&#160;main.ino']]],
  ['loop',['loop',['../serialForMega_8ino.html#afe461d27b9c48d5921c00d521181f12f',1,'loop():&#160;serialForMega.ino'],['../kauhis__mega_8ino.html#afe461d27b9c48d5921c00d521181f12f',1,'loop():&#160;kauhis_mega.ino'],['../bleToSerial__2_8ino.html#afe461d27b9c48d5921c00d521181f12f',1,'loop():&#160;bleToSerial_2.ino'],['../group__mega2560.html#gafe461d27b9c48d5921c00d521181f12f',1,'loop():&#160;main.ino']]]
];

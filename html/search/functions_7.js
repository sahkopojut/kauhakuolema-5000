var searchData=
[
  ['leg',['leg',['../classleg.html#a4d20b7f832acabf6216dcb6fb7ecc8c2',1,'leg::leg(int nTip_len, int nMid_len, int nRest_height, int nWalk_clearance)'],['../classleg.html#a418877dece641560d83d01fa236ec8a4',1,'leg::leg()']]],
  ['log',['log',['../group__mega2560.html#ga86e09be2bbc3f5a3b9f916f910c645b4',1,'log(T loggable):&#160;legs.ino'],['../legs_8ino.html#a86e09be2bbc3f5a3b9f916f910c645b4',1,'log(T loggable):&#160;legs.ino'],['../group__mega2560.html#ga86e09be2bbc3f5a3b9f916f910c645b4',1,'log(T loggable):&#160;main.ino']]],
  ['logln',['logln',['../group__mega2560.html#ga380cd75a227b957715da0e24ef3e1f46',1,'logln(T loggable):&#160;legs.ino'],['../legs_8ino.html#a380cd75a227b957715da0e24ef3e1f46',1,'logln(T loggable):&#160;legs.ino'],['../group__mega2560.html#ga380cd75a227b957715da0e24ef3e1f46',1,'logln(T loggable):&#160;main.ino']]],
  ['loop',['loop',['../serialForMega_8ino.html#afe461d27b9c48d5921c00d521181f12f',1,'loop():&#160;serialForMega.ino'],['../kauhis__mega_8ino.html#afe461d27b9c48d5921c00d521181f12f',1,'loop():&#160;kauhis_mega.ino'],['../bleToSerial__2_8ino.html#afe461d27b9c48d5921c00d521181f12f',1,'loop():&#160;bleToSerial_2.ino'],['../group__mega2560.html#gafe461d27b9c48d5921c00d521181f12f',1,'loop():&#160;main.ino']]]
];

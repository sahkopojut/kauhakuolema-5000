var searchData=
[
  ['sendcommands',['sendCommands',['../bleToSerial__2_8ino.html#a0a5c7283828c9c5f65940da58f1057db',1,'bleToSerial_2.ino']]],
  ['serialdebugging',['serialDebugging',['../kauhis__mega_8ino.html#adf1e8b75599793d40138052296882f16',1,'kauhis_mega.ino']]],
  ['setbaud',['setBaud',['../SerialStuff_8cpp.html#a8e83ed2fa6ebd5313fd27151e09fd27e',1,'SerialStuff.cpp']]],
  ['setdatasize',['setDataSize',['../SerialStuff_8cpp.html#ac8ae948379e58009165166b8a1182b2c',1,'SerialStuff.cpp']]],
  ['sethome',['setHome',['../classleg.html#a1da6da7499371e05ada071fba53de2d9',1,'leg']]],
  ['setlineparameters',['setLineParameters',['../SerialStuff_8cpp.html#aab1be1c41232af2beac0afd5b8796243',1,'SerialStuff.cpp']]],
  ['setmoveexectime',['setMoveExecTime',['../bleToSerial__2_8ino.html#a8f42144dc8164157c4394dc420781c20',1,'bleToSerial_2.ino']]],
  ['setnext',['setNext',['../classleg.html#a59944a94d7dddd3dc177065b862c0e60',1,'leg']]],
  ['setparity',['setParity',['../SerialStuff_8cpp.html#a43d65db60d7c62ed3e3b356e5b80efd9',1,'SerialStuff.cpp']]],
  ['setpins',['setPins',['../classleg.html#a8658156d028ca814567a008626d96900',1,'leg']]],
  ['setpos',['setPos',['../classleg.html#a454b6703128062672da82b89b534f39d',1,'leg']]],
  ['setprev',['setPrev',['../classleg.html#aa1ca6215bb881ffe79de81ff4b1a280c',1,'leg']]],
  ['setservo',['setServo',['../group__mega2560.html#ga37b4a24e061371f3ce6e7ae2a0731b7c',1,'main.ino']]],
  ['setstopbits',['setStopBits',['../SerialStuff_8cpp.html#a0aa87f181a5a9db0ca312b227e6cb825',1,'SerialStuff.cpp']]],
  ['setup',['setup',['../serialForMega_8ino.html#a4fc01d736fe50cf5b977f755b675f11d',1,'setup():&#160;serialForMega.ino'],['../kauhis__mega_8ino.html#a4fc01d736fe50cf5b977f755b675f11d',1,'setup():&#160;kauhis_mega.ino'],['../bleToSerial__2_8ino.html#a4fc01d736fe50cf5b977f755b675f11d',1,'setup():&#160;bleToSerial_2.ino'],['../group__mega2560.html#ga4fc01d736fe50cf5b977f755b675f11d',1,'setup():&#160;main.ino']]],
  ['setuplegs',['setupLegs',['../bleToSerial__2_8ino.html#af604abe2c6b525e83abcdeae713d7bf8',1,'bleToSerial_2.ino']]],
  ['setupservos',['setupServos',['../kauhis__mega_8ino.html#a746ba1b6d655515db7e15953e820ae70',1,'setupServos():&#160;kauhis_mega.ino'],['../kauhis__mega_8ino.html#a5f38a5b8eb24e046400c1cdcc24cce9a',1,'setupServos(int *ports):&#160;kauhis_mega.ino']]],
  ['stand',['stand',['../walkCycles_8hpp.html#aab5f090a24c6992c46b2f33fa57c1975',1,'stand(leg *legs, int leg_num):&#160;walkCycles.ino'],['../walkCycles_8ino.html#aab5f090a24c6992c46b2f33fa57c1975',1,'stand(leg *legs, int leg_num):&#160;walkCycles.ino']]],
  ['stopitnow',['stopItNow',['../group__mega2560.html#ga795b7104787aec3b6bc2f9f27e7ed3f8',1,'main.ino']]]
];

var searchData=
[
  ['serial_5fcmd_5fleft',['serial_cmd_left',['../bleToSerial__2_8ino.html#ab243435d1afa65d98245e78da01496a1',1,'bleToSerial_2.ino']]],
  ['serialreceiveinterval',['serialReceiveInterval',['../group__mega2560.html#ga1f51e2c5b0d497e0dce6764516f218c6',1,'main.ino']]],
  ['serialsemaphore',['SerialSemaphore',['../kauhis__mega_8ino.html#a76e269cce699229f8a9fa53bd1f9c03d',1,'kauhis_mega.ino']]],
  ['servo_5fports',['servo_ports',['../kauhis__mega_8ino.html#a533335f8aae1709a4ac9ce02b204bc00',1,'kauhis_mega.ino']]],
  ['servo_5fvalues',['servo_values',['../group__mega2560.html#ga4f564b551c8e0c146b86522827e43fba',1,'main.ino']]],
  ['servoports',['servoPorts',['../group__mega2560.html#ga857764ad6f9e52a80dfa1b084d819d78',1,'main.ino']]],
  ['servos',['servos',['../kauhis__mega_8ino.html#a037155bb522ce8c4127d1da95fe7d38c',1,'servos():&#160;kauhis_mega.ino'],['../group__mega2560.html#ga037155bb522ce8c4127d1da95fe7d38c',1,'servos():&#160;main.ino']]],
  ['start_5fmove',['start_move',['../group__mega2560.html#gac6274f2626e35072768184e5cf989baa',1,'main.ino']]]
];

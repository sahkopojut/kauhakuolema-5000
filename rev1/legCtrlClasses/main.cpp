/**
\file

*/

#include <iostream>
#include <math.h>
#include <cstdlib>

using namespace std;

/**
\class leg

\brief model one leg, as globally workable as possible.
*/

class leg{
private:

char tip;
char mid;
char base;

//angle offsets of leg joints (over-extension and reduced inward bend of leg for example)
///positive tip offset makes the leg over-extend at the cost of reduced inward bend
char offset_tip;
char offset_mid;

int tip_len;        //length of tip bit of leg
int mid_len;        //length of middle bit of leg
int rest_height;    //minimum distance between axis of mid servo and ground
int walk_clearance; //clearance between chassis and ground
int mid_axle_height;//rest_height + walk_clearance for calculations

int max_reach;      //maximum reach of a leg from the mid servo axis to the tip, depends on walk clearance!!!

double tip_len_squared;
double mid_len_squared;
//for cosine-statement in tip_by_extension()
double tip_mid_squared_sum;
double tip_mid_sum_2;

public:
/**
    @param nTip_len length of tip section of leg in mm
    @param nMid_len length of mid section of leg in mm
    @param nRest_height height of mid servo's axle from ground when chassis touches ground
    @param nWalk_clearance clearance between chassis and ground when walking
*/
leg(int nTip_len, int nMid_len, int nRest_height, int nWalk_clearance){

    //offset_tip =

    tip_len = nTip_len;
    mid_len = nMid_len;
    rest_height = nRest_height;
    walk_clearance = nWalk_clearance;
    //calculate maximum reach based on walk clearance
    max_reach = sqrt(pow(tip_len+mid_len,2)+pow(rest_height+walk_clearance,2));
    //mid bit inner axle height for calculations:
    mid_axle_height = walk_clearance + rest_height;
    //pre-calculate stuff for trig functions (trying to save time)
    tip_len_squared = pow(tip_len, 2);
    mid_len_squared = pow(mid_len, 2);
    tip_mid_squared_sum = tip_len_squared + mid_len_squared;
    tip_mid_sum_2 = (tip_len * mid_len)*2;

}
/**
@function
@returns value for tip servo based on requested extension (between abs(tip_len-mid_len) and tip_len+mid_len)
*/
int tip_by_extension(unsigned char extension){

    if(extension < abs(tip_len-mid_len) || extension > tip_len+mid_len){
        std::cout << "impossibru!" << std::endl;
        return 255;
    }else{

        double sum = tip_mid_squared_sum - pow(extension, 2);
        //std::cout << sum << "/" << tip_mid_sum_2 << std::endl;
        sum = acos(sum/tip_mid_sum_2);
        std::cout << sum << std::endl; //

        return (int)round(sum/0.017453293);

    }

}

/**
@function
calculates angle of mid leg servo based on desired reach in mm ()
*/
int angle_of_extension(int reach){

    if(reach > max_reach){
        std::cout << "The Madmen of the Reach! (lower clearance to gain reach)" << std::endl;
        return 255;
    }
    else{
    //std::cout << "atan(" << reach << "/" << mid_axle_height << ")" << std::endl;
         double res = atan(reach/mid_axle_height);
      //  std::cout << "angle of extension:" << res/0.017453293 << std::endl;
        return (int)round(res/0.017453293);
    }
}
/**
    calculate the hypotenuse (extension of effective "leg") by desired reach from axle, at walking clearance
*/
int extension_by_reach(int reach){
    if(reach > max_reach){
        std::cout << "The Madmen of the Reach! (lower clearance to gain reach)" << std::endl;
        return 255;
    }
    else{
    return sqrt(pow(mid_axle_height,2)+pow(reach,2));
    }
}
/**
    calculate correct servo setting for a desired extension angle (0 degrees equals straight down at the mid servo)
*/
int mid_servo_angle(int ext_angle, int extension){

double alpha = acos(mid_len_squared - tip_len_squared + pow(extension, 2));

return 1;
}
};

int main(int argc,char **argv)
{
    leg spoderleg(120, 74, 40, 20);
    int tip_angle = 0;
    int mid_angle = 0;
    int reach = 120;

    if(argc > 1){

    reach = atoi(argv[1]);
    int extension = spoderleg.extension_by_reach(reach);
    std::cout << extension << std::endl;
    tip_angle = spoderleg.tip_by_extension(extension);
    mid_angle = spoderleg.angle_of_extension(reach);

    std::cout << "a reach of "<< reach << " mm outward from the axle of mid and at walking height is achieved by writing ";
    std::cout << tip_angle << " to the tip servo and " << mid_angle << "to the mid servo" << std::endl;
    }else{

    std::cout <<"give desired reach next time!" << std::endl;
    }




    return 0;
}

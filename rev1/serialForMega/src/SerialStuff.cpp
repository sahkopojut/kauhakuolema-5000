/*
 * SerialStuff.cpp
 *
 * Created: 13.11.2015 13:50:43
 *  Author: s11300
 */
	//craeting serial connection: first, collect all parameters in register dummies, then enact the changes on actual registers.

#include <stdio.h>
#include <avr/io.h>
//#include <util/delay.h>
#include <stdarg.h>
//#include "ledCommander.h"
#include "bitwiseMacros.h"
#include "SerialStuff.h"
//#include "LAB4Common.h"
#include "circular.h"

//#define CheckBit(target, bit)	{return target & (1<<bit);}

bool regA[8] = {0};
bool regB[8]= {0};
bool regC[8]= {0};

	//extern circular TX(64);
	//extern circular RX(64);


volatile unsigned ubrrH, ubrrL; //globals to set the UBRR

//lookups are easier, almost always.
unsigned long baudLookUp[15] = {3332, 1666, 832, 416, 207, 103, 68, 51, 34, 25, 16, 12, 8, 3, 3};
unsigned long baudLookUpX2U[15] = {6666, 3332, 1666, 832, 416, 207, 138, 103, 68, 51, 34, 25, 16, 8, 7};
unsigned long baudRates[15] = {300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 57600, 78600, 115200, 230400, 250000};

//
///////////////
//circ buffer RX

extern volatile unsigned char RXBuffer[RXBUFSIZE]; // input buffer for interrupt-driven serial comms
extern volatile unsigned RXIn;	// input buffer iterator
extern volatile unsigned RXOut;	// input buffer iterator

//circ buffer RX
///////////////
//circ buffer TX

extern volatile unsigned char TXBuffer[RXBUFSIZE]; // input buffer for interrupt-driven serial comms
extern volatile unsigned TXIn;	// input buffer iterator
extern volatile unsigned TXOut;	// input buffer iterator

//circ buffer TX
///////////////

void setDataSize( unsigned dataSize) //always works
{


		if(dataSize == 5)
		{
			regB[2] = false;
			regC[2] = false;
			regC[1] = false;
			/*ClearBit(UCSR0B, UCSZ20);
			ClearBit(UCSR0C, UCSZ01);
			ClearBit(UCSR0C, UCSZ00);*/
		}
		else if(dataSize == 6)
		{
			regB[2] = false;
			regC[2] = false;
			regC[1] = true;
			/*ClearBit(UCSR0B, UCSZ20);
			ClearBit(UCSR0C, UCSZ01);
			SetBit(UCSR0C, UCSZ00);*/
			//UCSR0C = _BV(UCSZ00);
		}
		else if(dataSize == 7)
		{
			regB[2] = false;
			regC[2] = true;
			regC[1] = false;
			/*ClearBit(UCSR0B, UCSZ20);
			SetBit(UCSR0C, UCSZ01);
			ClearBit(UCSR0C, UCSZ00);*/
			//UCSR0C = _BV(UCSZ01);
		}
		else if(dataSize == 8)
		{
			regB[2] = false;
			regC[2] = true;
			regC[1] = true;
			/*ClearBit(UCSR0B, UCSZ20);
			SetBit(UCSR0C, UCSZ01);
			SetBit(UCSR0C, UCSZ00);*/
			//UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);
		}
		else if(dataSize == 9)
		{
			regB[2] = true;
			regC[2] = true;
			regC[1] = true;
				/*SetBit(UCSR0B, UCSZ20);
				SetBit(UCSR0C, UCSZ01);
				SetBit(UCSR0C, UCSZ00);*/
				//UCSR0B = _BV(UCSZ20);
				//UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);
		}
		else
		{
				//error

		}



}

void setParity( unsigned parity) // 0 = none, 1 = odd, 2 = even
{




		if(parity == 1)
		{
			regC[4] = true;
			regC[5] = true;
			//
			//SetBit(UCSR0C, UPM01);
			//SetBit(UCSR0C, UPM00);
			//&UCSR0C = _BV(UPM00) | _BV(UPM01);
		}
		else if(parity == 2)
		{
			regC[4] = false;
			regC[5] = true;
			//
			//ClearBit(UCSR0C, UPM00);
			//SetBit(UCSR0C, UPM01);
			//UCSR0C = _BV(UPM01);
		}
		else if(parity == 0)
		{
			regC[4] = false;
			regC[5] = false;
			//
			//ClearBit(UCSR0C, UPM00);
			//ClearBit(UCSR0C, UPM01);
		}


}

void setStopBits(unsigned stops) // 1 or 2 stop bits
{
	if(stops == 1)
	{
		regC[3] = true;
		//SetBit(UCSR0C, USBS0);
	}
	if(stops == 2)
	{
		regC[3] = false;
		//ClearBit(UCSR0C, USBS0);
	}
}

void RXEn(bool state)
{
	if(state == true)
	{
		regB[4] = true;
		regB[1] = true;
		//SetBit(UCSR0B, RXEN0);
		//SetBit(UCSR0B, RXB80);
	}
	if(state == false)
	{
		regB[4] = false;
		regB[1] = false;
		//ClearBit(UCSR0B, RXEN0);
		//ClearBit(UCSR0B, RXB80);
	}
}

void TXEn(bool state)
{
	if(state == true)
	{
		regB[3] = true;
		regB[0] = true;
		//SetBit(UCSR0B, TXEN0);
		//SetBit(UCSR0B, TXB80);
	}
	if(state == false)
	{
		regB[3] = false;
		regB[0] = false;
		//ClearBit(UCSR0B, TXEN0);
		//ClearBit(UCSR0B, TXB80);
	}
}

void setBaud(unsigned long baud)//works only on 16MHz processor, obviously
{
	unsigned long ubrr = 0;

	for(int i = 0;i < 15; i++)
	{
		if(baud == baudRates[i])
		{
			if(baud >=4800)
			{
				ubrr = baudLookUpX2U[i];
				regA[1] = true;
				//SetBit(UCSR0A, U2X0);
			}
			else
			{
				ubrr = baudLookUp[i];
				regA[1] = false;
				//ClearBit(UCSR0A, U2X0);
			}

		}
	}


	if(ubrr != 0)
	{
		ubrrH = ubrr >> 8;
		ubrrL = ubrr;

	/*Do this in Enact()
		UBRR0H = ubrrH;
		UBRR0L = ubrrL;
		*/

	}
}

/*
void uart_putchar(char c, unsigned port) {
    loop_until_bit_is_set(UCSR0A, UDRE0);// Wait until data register empty.
    UDR0 = c;	//set outgoing bit.
}

char uart_getchar(unsigned port) {

	switch(port)
	{
		case 0:
    loop_until_bit_is_set(UCSR0A, RXC0);
	return UDR0;

	break;
		case 1:
    loop_until_bit_is_set(UCSR1A, RXC1);
	return UDR1;
	break;
		case 2:
    loop_until_bit_is_set(UCSR2A, RXC2);
	return UDR2;
	break;
		case 3:
    loop_until_bit_is_set(UCSR3A, RXC3);
	return UDR3;

	break;
	}

}*/
/*
int characterReady(unsigned port)
{
	switch(port)
	{
		case 0:
		if(CheckBit(UCSR0A, RXC0)){return true;}
		else {return false;}
		break;
		case 1:
		if(CheckBit(UCSR1A, RXC1)){return true;}
		else {return false;}
		break;
		case 2:
		if(CheckBit(UCSR2A, RXC2)){return true;}
		else {return false;}
		break;
		case 3:
		if(CheckBit(UCSR3A, RXC3)){return true;}
		else {return false;}

		break;
	}
}
*//*
char readCharacter(unsigned port)
{
	switch(port)
	{
		case 0:
    //loop_until_bit_is_set(UCSR0A, RXC0);
	return UDR0;
	break;
		case 1:
    //loop_until_bit_is_set(UCSR1A, RXC1);
	return UDR1;
	break;
		case 2:
    //loop_until_bit_is_set(UCSR2A, RXC2);
	return UDR2;
	break;
		case 3:
    //loop_until_bit_is_set(UCSR3A, RXC3);
	return UDR3;

	break;
	}
}


int putsSerial(unsigned port, char *buffer) //returns length of transmitted string
{
	int index = 0;

	if(buffer[0] != '\0') //check for real string
	{
		while(buffer[index] != '\0')
		{
			uart_putchar(buffer[index], port);
			index++;
		}

		uart_putchar('\0', port);
		return index;
	}
	else return 0;
}

int getsSerial(unsigned port, char*buffer) //returns length of received string
{
	    char charGuy;
		bool breaker = false;
	//unsigned char nope = NULL;
	unsigned int index = 0;

	if(characterReady(port))
	{
		while (!breaker)
		{
				charGuy = uart_getchar(port); //read the first char

				if(charGuy != '\n' && charGuy != '\r' && charGuy != '\0') //common endings
				{
					buffer[index] = charGuy;
					index++;

				}
				else
				{
					breaker = true;
				}
		}


			buffer[index] = '\0'; // null terminated string
			return index; //return number of received characters
	}
	else return 0;
}
*/

/*
int vsscanf(const char *s, const char *fmt, va_list ap) //shitty version of vsscanf, only 7 %-specs...
{
  void *a[20];
  int i;

  for (i=0; i<sizeof(a)/sizeof(a[0]); i++)
  {a[i] = va_arg(ap, void *);}

  return sscanf(s, fmt, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15], a[16], a[17], a[18], a[19]);
}*/
/*
int printFormatted(unsigned port, const char * format , ...)
{
	char buffer[128];
	va_list arg;
	//int num;

	va_start(arg, format);

	//
	//vsscanf(string, format, arg);
	vsprintf(buffer, format, arg);

	putsSerial(port, buffer);

	va_end(arg);
}



void getStuff(const char *str, const char* format, ...)
{
	va_list arg;
	va_start(arg, format);
	vsscanf(str, format, arg);
	va_end(arg);
}

int scanFormatted(unsigned port, char* buffer, const char* format, ...) //returns number of characters received from serial port
{
	//char buffer[64];
	//char buffer2[64] = {"12 ff heippa"};

	int num = 0;
	num = getsSerial(port, buffer);

	va_list arg;
	va_start(arg, format);

	vsscanf(buffer, format, arg);


	va_end(arg);

	return num;
}
*/


void enactLineParameters(unsigned port) // now on all serial ports!
{
	for(int i = 0; i<8; i++)
	{
		switch(port)
		{
			case 0: //set the bits according to reg dummies
			{ // really clumsy way to do this, but works
				if(regA[i]){SetBit(UCSR0A, i);}
				else{ClearBit(UCSR0A, i);}
				if(regB[i]){SetBit(UCSR0B, i);}
				else{ClearBit(UCSR0A, i);}
				if(regC[i]){SetBit(UCSR0C, i);}
				else{ClearBit(UCSR0A, i);}

				UBRR0H = ubrrH;
				UBRR0L = ubrrL;
				break;
			}
			case 1:
			{
				if(regA[i]){SetBit(UCSR1A, i);}
				else{ClearBit(UCSR0A, i);}
				if(regB[i]){SetBit(UCSR1B, i);}
				else{ClearBit(UCSR0A, i);}
				if(regC[i]){SetBit(UCSR1C, i);}
				else{ClearBit(UCSR0A, i);}

				UBRR1H = ubrrH;
				UBRR1L = ubrrL;
				break;
			}
			case 2:
			{
				if(regA[i]){SetBit(UCSR2A, i);}
				else{ClearBit(UCSR0A, i);}
				if(regB[i]){SetBit(UCSR2B, i);}
				else{ClearBit(UCSR0A, i);}
				if(regC[i]){SetBit(UCSR2C, i);}
				else{ClearBit(UCSR0A, i);}

				UBRR2H = ubrrH;
				UBRR2L = ubrrL;
				break;
			}
			case 3:
			{
				if(regA[i]){SetBit(UCSR3A, i);}
				else{ClearBit(UCSR0A, i);}
				if(regB[i]){SetBit(UCSR3B, i);}
				else{ClearBit(UCSR0A, i);}
				if(regC[i]){SetBit(UCSR3C, i);}
				else{ClearBit(UCSR0A, i);}

				UBRR3H = ubrrH;
				UBRR3L = ubrrL;
				break;
			}

		}
	}
}


void setLineParameters(unsigned port, unsigned long speed, unsigned parity, unsigned cDataBits, unsigned cStopBits)
{
	//only port 0 is connected to usb-to-serial!
	setBaud(speed);
	setParity(parity);
	setDataSize(cDataBits);
	setStopBits(cStopBits);
	RXEn(true);
	TXEn(true);

	enactLineParameters(port); // enact the chosen line params on port (actually write registers)
}
/*
unsigned long findBaudRate(char trigger)//see if the other connectee is sending 'letter', if not, swap baud rate
{
	volatile char charGuy;
	int index = -1;
	do
	{
		index++;
		setLineParameters(0, baudRates[index], 0, 8, 1);
		enactLineParameters(0);
		charGuy = uart_getchar(0);
		uart_putchar(charGuy,0);
		if(index >=15)
		{
			index = -1;
		}

	} while (charGuy != 'trigger');

	return baudRates[index];

}
*/

	/////////////////////////////////////////////////Interrupt-driven comms from here on out//////////////////////////////////////////

	int printFormatted_Async(unsigned port, const char * format , ...) //write formatted data to serial via the interrupt routine
{
	char buffer[128];
	va_list arg;
	//int num;

	va_start(arg, format);

	//
	//vsscanf(string, format, arg);
	vsprintf(buffer, format, arg);

	//putsSerial(port, buffer);
	TXBufWrite(buffer, 128);
	//TX.writeFrom(buffer);
	va_end(arg);

	return 0;
}

	void enableRCInterrupt() //enable the incoming data and empty output register interrupts
	{
		UCSR0B |= (1 << RXCIE0);
		UCSR0B |= (1 << UDRIE0);
	}

void TXBufpush(char val)//pushes new character into TX buffer, handles head and tail
{
	TXBuffer[TXIn] = val;

	if(TXIn >= TXBUFSIZE - 1){TXIn = 0;}
	else{TXIn++;}
}

char TXBufpop()//returns the next character in TX buffer, handles the head and tail
{
	if(TXOut != TXIn)
	{
		char localChar = TXBuffer[TXOut];
		if(TXOut >= TXBUFSIZE - 1){TXOut = 0;}
		else{TXOut++;} //increment buffer output iterator

		return localChar;
	}
	else
	{
		return 3;
	}
}



int RXCheck() //returns the number of characters in RX buffer
{
	if(RXIn == RXOut) return 0;

	if(RXIn > RXOut)
	{
		return RXIn - RXOut;
	}
	else
	{
		return (RXBUFSIZE - RXOut) + RXIn;
	}

}

char RXBufpop()//returns the next character in RX buffer, handles the head and tail
{
	if(RXOut != RXIn)
	{
		char localChar = RXBuffer[RXOut];
		if(RXOut >= RXBUFSIZE - 1){RXOut = 0;}
		else{RXOut++;} //increment buffer output iterator

			return localChar;
	}
	else
	{
		return 3;
	}
}

void RXBufpush(char val)//pushes new character into RX buffer, handles head and tail
{
	RXBuffer[RXIn] = val;

	if(RXIn >= RXBUFSIZE - 1){RXIn = 0;}
	else{RXIn++;}
}

void TXBufWrite(char *buffer, int buffersize)//send string to buffer
{
	while (*buffer != '\n')
	{

		TXBufpush(*buffer);
		buffer++;
	}

	TXBufpush('\n');
}

int RXRead(char* buffer) //read the RX buffer into (parameter char array)
{
	int index = 0;
	char local = RXBufpop();
	if(local == 3)
	{
		return 0;
	}
	else
	{
		while(local != 3)
			{
				buffer[index] = local;
				index++;
				local = RXBufpop();
			}
			return 1;
	}
}

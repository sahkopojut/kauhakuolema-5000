/*
 * SerialStuff.h
 *
 * Created: 13.11.2015 13:34:12
 *  Author: s11300
 */ 

#ifndef SERIALSTUFF_H_
#define SERIALSTUFF_H_


#define SERIAL_OVER_USB 0


//# define F_CPU 16000000




#define BAUD_300 0
#define BAUD_600 1
#define BAUD_1200 2
#define BAUD_2400 3
#define BAUD_4800 4

#define BAUD_9600 5
#define BAUD_14400 6
#define BAUD_19200 7
#define BAUD_28800 8
#define BAUD_38400 9

#define BAUD_57600 10
#define BAUD_76800 11
#define BAUD_115200 12
#define BAUD_230400 13
#define BAUD_250000 14

#ifndef RXBUFSIZE

#define RXBUFSIZE 64
#define TXBUFSIZE 64
#endif

unsigned long findBaudRate(char trigger);
void setLineParameters(unsigned port, unsigned long speed, unsigned parity, unsigned cDataBits, unsigned cStopBits);
void enactLineParameters(unsigned port);
int scanFormatted(unsigned port, char* buffer, const char* format, ...);
void getStuff(const char *str, const char* format, ...);
int printFormatted(unsigned port, const char * format , ...);
int vsscanf(const char *s, const char *fmt, va_list ap);
int getsSerial(unsigned port, char*buffer);
int putsSerial(unsigned port, char *buffer);
char readCharacter(unsigned port);
int characterReady(unsigned port);
char uart_getchar(unsigned port);
void uart_putchar(char c, unsigned port);
void setBaud(unsigned long baud);
void TXEn(bool state);
void RXEn(bool state);
void setStopBits(unsigned stops);
void setParity( unsigned parity);
void setDataSize( unsigned dataSize);

//////////lab7
int printFormatted_Async(unsigned port, const char * format , ...) ;
///////////
int RXRead(char* buffer); //read the RX buffer into (parameter char array)
void TXBufWrite(char *buffer, int buffersize);//send string to buffer
void RXBufpush(char val);
char RXBufpop();
int RXCheck();
char TXBufpop();
void TXBufpush(char val);
	void enableRCInterrupt();

#endif /* SERIALSTUFF_H_ */
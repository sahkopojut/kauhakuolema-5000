/*
 * bitwiseMacros.h
 *
 * Created: 13.11.2015 13:48:23
 *  Author: s11300
 */ 


#ifndef BITWISEMACROS_H_
#define BITWISEMACROS_H_


//helper macros
#define SetBit(target, bit)		{target |= (1 << bit);}
#define ClearBit(target, bit)	{target &= ~(1<<bit);}
#define ToggleBit(target, bit)	{target ^= (1<<bit);}




#endif /* BITWISEMACROS_H_ */
#include "circ.hpp"

circ::circ(int nBufLen){
  Buffer = new unsigned char[nBufLen];
  //init vars
  BufferLen = nBufLen;
  Head = 0;
  Tail = 0;
}
//put one in
void circ::push(char toSend){

Buffer[Head] = toSend;

  if(Head >= BufferLen - 1){
    Head = 0;
  }else{
    Head++;
  }

}
//take one out
char circ::pop(){
  if(Tail != Head){//hasn't caught up with head

    char popped = Buffer[Tail];
    if(Tail >= BufferLen -1){
      Tail = 0;
    }
    else{
      Tail++;
    }
    return popped;

  }
  else{
    return 3; //end of text sign
  }
}
//write c-string to interrupt-powered buffer
void circ::write(char*buffer){
  while(*buffer != '\n'){
    this->push(*buffer);
    buffer++;
  }
}
//read c-string from interrupt-filled buffer
bool circ::read(char*buffer){
int i = 0;
char popped = this->pop();
if(popped != 3){
  while(popped!=3){
    buffer[i] = popped;
    popped = this->pop();
    i++;
  }
  return 1;
}
else return 0;

}

//returns the number of characters in  buffer
int circ::available()
{
	if(Head == Tail) return 0;

	if(Head > Tail)
	{
		return Head - Tail;
	}
	else
	{
		return (BufferLen - Tail) + Head;
	}

}

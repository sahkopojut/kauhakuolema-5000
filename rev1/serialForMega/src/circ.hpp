#ifndef CIRC
#define CIRC

class circ{
private:

  volatile unsigned char *Buffer;

  volatile int BufferLen;

  volatile int Head;
  volatile int Tail;

public:

  circ(int BufLen);

  void push(char toSend);//put one in
  char pop();//take one out

  void write(char*buffer);//write c-string to interrupt-powered buffer
  bool read(char*buffer);//read c-string from interrupt-filled buffer
  int available();


};


#endif

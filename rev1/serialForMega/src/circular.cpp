/*
 * circular.cpp
 *
 * Created: 2.2.2016 13:11:07
 *  Author: s11300
 */ 
#include <stdio.h>
#include <avr/io.h>
//#include <util/delay.h>
#include <stdarg.h>
#include <stdlib.h>
#include "circular.h"

circular::circular(int nsize)
{
	size = nsize;
	buffer = (char*)calloc(size, 1);
	tail = 0;
	head = 1;
}

circular::~circular()
{
	
}

void circular::push(char addition)
{
	buffer[head] = addition;
	if(head >= size-1){head = 0;}
	else {head++;}
}

char circular::pop()
{
	if(head != tail)
	{
		if(tail >= size-1){tail = 0;}
		else {tail++;}
		return buffer[tail - 1];
	}
}

int circular::check()
{
	if(head == tail) return 0;
	
	else if(head > tail)
	{
		return head - tail;
	}
	else if(tail > head)
	{
		return (size - tail) + head;
	}
}

void circular::readInto(char* buffer)
{
	int index = 0;
	while(this->check())
	{
		buffer[index] = this->pop();
		index++;
	}
}
void circular::writeFrom(char *buffer)
{
	while (*buffer != NULL)
	{
		
		this->push(*buffer);
		buffer++;
	}
	
}
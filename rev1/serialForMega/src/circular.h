/*
 * circular.h
 *
 * Created: 2.2.2016 13:11:21
 *  Author: s11300
 */ 


#ifndef CIRCULAR_H_
#define CIRCULAR_H_


class circular
{
	volatile char* buffer;
	volatile int head;
	volatile int tail;
	volatile int size;
	
	public:
	circular(int nsize);
	~circular();
	void readInto(char* buffer);
	int check();
	char pop();
	void push(char addition);
	void writeFrom(char *buffer);
	
	
	
};


#endif /* CIRCULAR_H_ */
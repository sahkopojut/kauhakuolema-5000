#include <Arduino.h>

#include "bitwiseMacros.h"
#include "SerialStuff.h"

#include "circ.hpp"

circ RXBuf(64);
circ TXBuf(64);
///////////////
//circ buffer RX

volatile unsigned char RXBuffer[RXBUFSIZE]; // input buffer for interrupt-driven serial comms
volatile unsigned RXIn;	// input buffer iterator
volatile unsigned RXOut;	// input buffer iterator

//circ buffer RX
///////////////
//circ buffer TX

volatile unsigned char TXBuffer[RXBUFSIZE]; // input buffer for interrupt-driven serial comms
volatile unsigned TXIn;	// input buffer iterator
volatile unsigned TXOut;	// input buffer iterator
/////////////////////////////////////////////USART ISR vectors//////////////////////////////////
ISR(USART0_RX_vect)//catch incoming
{

	volatile unsigned char val = UDR0;
	//filter control chars
	if(val >= 32 )
	{
		//RXBufpush(val);
		RXBuf.push(val);
	}

}

ISR(USART0_UDRE_vect)//send outgoing
{
	if(TXBuf.available()>0)
	{

      UDR0 =  TXBuf.pop();//TXBufpop();

  }



}

/**
  parses a zstring into command tokens, returns number of tokens found
*/
int parseCommand(char *str , String tokens[]){
  int i =0;
  strtok(str, ' ');
  while(str){
    i++;
    tokens[i] = strtok(NULL, ' ');

  }
  return i;
}

void setup() {
  // put your setup code here, to run once:
  setLineParameters(0, 9600,  0, 8, 1);
  enableRCInterrupt();
}

void loop() {
	delay(150);
	char buffer[64] = {0};
	int i =0;

		if(RXBuf.available()){
			int num = RXBuf.available();

			/*itoa(num, buffer, 10);
			while(buffer[i]!=0){
				TXBuf.push(buffer[i]);
				i++;
			}*/

			for(int i=0; i<num; i++){
				buffer[i] = RXBuf.pop();
			}
			buffer[num] = '\n';

			TXBuf.write(buffer);
		}



}

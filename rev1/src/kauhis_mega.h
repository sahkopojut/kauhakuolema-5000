#ifndef _KAUHIS_MEGA
#define _KAUHIS_MEGA
/**
preprocessor defines
*/
#define FRONT_TIP 0
#define FRONT_MID 1
#define FRONT_BASE 2

#define MID_TIP 3
#define MID_MID 4
#define MID_BASE 5

#define BACK_TIP 6
#define BACK_MID 7
#define BACK_BASE 8

#define LEG_ID 0
#define FRONT 1
#define MID 2
#define BACK 3

#define TIP 1
#define MIDDLE 2
#define BASE 3

#endif

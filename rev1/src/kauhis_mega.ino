#include <Arduino.h>
#include <Servo.h>
#include <Wire.h>
//FreeRTOS headers
#include <Arduino_FreeRTOS.h>
#include <FreeRTOSConfig.h>
//#include <FreeRTOS_AVR.h>
#include <list.h>
#include <queue.h>
#include <semphr.h>
#include <task.h>
#include <timers.h>
//project header
#include "kauhis_mega.h"

#define MY_NAME "kauhis_left"
#define MY_NAME "kauhis_right"

#define RXCOMMAND__MAX_LENGTH 14//max length of RXCommand: 1:id, 2:space, 3,4,5:tip_pos, 6:space, 7,8,9:mid_pos, 10:space, 11,12,13:base_pos, 14:\n

//debug mode flag
bool _debugging = true;

Servo servos[9];
int servo_ports[9];




/**
Simple struct that holds the values of the desired leg position
*/
struct legMessage{
  int tip;
  int mid;
  int base;
}xlegMessage;

/**
FreeRTOS input queue handles for each leg's handler task
*/
QueueHandle_t queueHandle_Leg_Front;
QueueHandle_t queueHandle_Leg_Mid;
QueueHandle_t queueHandle_Leg_Back;

SemaphoreHandle_t SerialSemaphore;
/*
static functions
*/
bool checkSerial(unsigned char *msg);
void setupServos();
//specify the 9 ports to use for the legs
void setupServos(int *ports);



void _moveleg(char* message, int len);
void _moveleg(char leg, char angle_tip, char angle_mid, char angle_base);

void serialDebugging(char* str, struct legMessage legMsg);
bool RXCommand(int *msg);


/*
FreeRTOS task that handles serial comms
*/
void TaskRXTX(void *pvParameters);
/*
own task for each leg for smooth operation
*/
void TaskLeg_Front(void *pvParameters);
void TaskLeg_Mid(void *pvParameters);
void TaskLeg_Back(void *pvParameters);

void setup(){

  //define servo bindings via these
  servo_ports[FRONT_TIP] = 13;
  servo_ports[FRONT_MID] = 12;
  servo_ports[FRONT_BASE] =11;

  servo_ports[MID_TIP] =3;
  servo_ports[MID_MID] =4;
  servo_ports[MID_BASE] =5;

  servo_ports[BACK_TIP] =6;
  servo_ports[BACK_MID] =7;
  servo_ports[BACK_BASE] =8;
//setup servo connections
setupServos(servo_ports);

//start serial comms with brain node(due)
Serial.begin(9600);
//wait for serial to init
while(!Serial){
  ;
}

//make the queues
queueHandle_Leg_Front = xQueueCreate( 10, sizeof( struct legMessage * ) );

queueHandle_Leg_Mid = xQueueCreate( 10, sizeof( struct legMessage * ) );

queueHandle_Leg_Back = xQueueCreate( 10, sizeof( struct legMessage * ) );

// Now set up tasks
 xTaskCreate(
   TaskRXTX
   ,  (const portCHAR *)"RXTX"   // A name just for humans
   ,  128  // This stack size can be checked & adjusted by reading the Stack Highwater
   ,  NULL
   ,  2  // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
   ,  NULL );

 xTaskCreate(
   TaskLeg_Front
   ,  (const portCHAR *) "Leg_Front"
   ,  128  // Stack size
   ,  NULL
   ,  1  // Priority
   ,  NULL );

 xTaskCreate(
   TaskLeg_Mid
   ,  (const portCHAR *) "Leg_Mid"
   ,  128  // Stack size
   ,  NULL
   ,  1  // Priority
   ,  NULL );

 xTaskCreate(
   TaskLeg_Back
   ,  (const portCHAR *) "Leg_Back"
   ,  128  // Stack size
   ,  NULL
   ,  1  // Priority
   ,  NULL );
//ready! task scheduler boots automatically

}

void loop() {
  // empty loop, RTOS handles scheduling
}


/*
FreeRTOS task that handles serial comms
*/
void TaskRXTX(void *pvParameters){
  (void) pvParameters;
  struct legMessage *pxMessage;

  for(;;){
    //message buffer
      int msg[4] = {0};
      bool messageReceived = false;
    //  if(checkSerial(msg));


    //if a message was received:
    messageReceived = RXCommand(msg);

    if(messageReceived){
      //populate message struct
      struct legMessage legMsg;
      legMsg.tip = msg[TIP];
      legMsg.mid = msg[MIDDLE];
      legMsg.base = msg[BASE];
      //send to appropriate leg handler task
      Serial.print(msg[TIP]);
      Serial.print(msg[MIDDLE]);
      Serial.println(msg[BASE]);

      pxMessage = & legMsg;
      //depending on content of ID field, parse requested leg position to appropriate handler task
      switch(msg[LEG_ID])
      {
        case FRONT: {
          xQueueSend( queueHandle_Leg_Front, ( void * ) &pxMessage, ( TickType_t ) 0 );
          break;
        }
        case MID: {
          xQueueSend( queueHandle_Leg_Mid, ( void * ) &pxMessage, ( TickType_t ) 0 );
          break;
        }
        case BACK: {
          xQueueSend( queueHandle_Leg_Back, ( void * ) &pxMessage, ( TickType_t ) 0 );
          break;
        }
        default: {break;}
      }
    }

    //allow the message to populate uart buffer (too little delay and the message is cut in pieces)
    vTaskDelay(25);
  }
}

/*
own task for each leg for smooth operation
*/
void TaskLeg_Front(void *pvParameters){
  (void) pvParameters;

  struct legMessage *pxRxedMessage;
  for(;;){
    if( queueHandle_Leg_Front != 0 )
    {
      if( xQueueReceive( queueHandle_Leg_Front, &( pxRxedMessage ), ( TickType_t ) 10) )
        {
          struct legMessage msg = *pxRxedMessage;

          //allow debugging of leg logic via serial terminal
          if(_debugging){
            //modify to work through due later on
            serialDebugging("in front leg!", msg);
          }
          //final servo handler
          _moveleg(FRONT, msg.tip, msg.mid, msg.base);
        }
    }
    vTaskDelay(1);
  }

}
/*
own task for each leg for smooth operation
*/
void TaskLeg_Mid(void *pvParameters){
  (void) pvParameters;
struct legMessage *pxRxedMessage;
  for(;;){
    if( queueHandle_Leg_Mid != 0 )
    {
      if( xQueueReceive( queueHandle_Leg_Mid, &( pxRxedMessage ), ( TickType_t ) 10) )
        {
          struct legMessage msg = *pxRxedMessage;

          //allow debugging of leg logic via serial terminal
          if(_debugging){
            //modify to work through due later on
            serialDebugging("in middle leg!", msg);
          }
          //final servo handler
          _moveleg(MID, msg.tip, msg.mid, msg.base);
        }
    }
vTaskDelay(1);
  }
}
/*
own task for each leg for smooth operation
*/
void TaskLeg_Back(void *pvParameters){
  (void) pvParameters;
struct legMessage *pxRxedMessage;
  for(;;){
    if( queueHandle_Leg_Back != 0 )
    {
      if( xQueueReceive( queueHandle_Leg_Back, &( pxRxedMessage ), ( TickType_t ) 10) )
        {
          struct legMessage msg = *pxRxedMessage;

          //allow debugging of leg logic via serial terminal
          if(_debugging){
            //modify to work through due later on
            //text adventure! debugging for logic
            serialDebugging("in back leg!", msg);
          }
          //final servo handler
          _moveleg(BACK, msg.tip, msg.mid, msg.base);
        }
    }
vTaskDelay(1);
  }
}

/**
attach each servo to port specified by array ports
ports must have 9 positions!!
*/
void setupServos(int *ports){
  for(int i =0; i<9; i++){
    servos[i].attach(ports[i]);
  }
}

/**
prints the result of command propagated to the leg handler
*/
void serialDebugging(char* str, struct legMessage legMsg){
  vTaskSuspendAll ();
  Serial.println(str);
  Serial.print("tip: ");
  Serial.println(legMsg.tip);

  Serial.print("mid: ");
  Serial.println(legMsg.mid);

  Serial.print("base: ");
  Serial.println(legMsg.base);
  Serial.flush();
  xTaskResumeAll ();
}

/**
hardware layer, send controls to the leg in question
*/
void _moveleg(char leg, char angle_tip, char angle_mid, char angle_base){

  switch(leg){
    case FRONT:{
      servos[FRONT_TIP].write(angle_tip);
      servos[FRONT_MID].write(angle_mid);
      servos[FRONT_BASE].write(angle_base);
      break;
    }
    case MID:{
      servos[MID_TIP].write(angle_tip);
      servos[MID_MID].write(angle_mid);
      servos[MID_BASE].write(angle_base);
      break;
    }
    case BACK:{
      servos[BACK_TIP].write(angle_tip);
      servos[BACK_MID].write(angle_mid);
      servos[BACK_BASE].write(angle_base);
      break;
    }
    default: break;
  }
}
/**
  if a command is found in the in buffer, return positive, else return negative
  if the command in input buffer consists of valid leg id and 3 parseable values between 0-180,
   separated by spaces,it is copied into the array pointed by the parameter *char msg

*/

bool RXCommand(int *msg){

if(Serial.available())
  {
    delay(25); //ensure full message
    int i=0;
    while(i<4)
    {
      if(Serial.available()){

        msg[i] = Serial.parseInt();

        Serial.print(i);
        Serial.print(" of msg: ");
        Serial.println(msg[i]);
        Serial.flush();
        i++;
        if( i == 4){
          return true;
        }
      }
      else if(i!=0){
        
          Serial.read();
        }
    }

  }

  }
  return false;
}

/*! \file bleToSerial_2.ino
\brief the main loop of the Arduino Due BLE to serial commands-controller

Helper functions and Arduino loop that parses commands from BLE-UART, keeps track
of the states of multiple leg objects and writes correctly formatted commands over
serial to the Mega2560s.
*/


#include <SPI.h>
#include <boards.h>
#include <RBL_nRF8001.h>
#include <Wire.h>
#include <Arduino.h>

#include "legs.hpp"
#include "walkCycles.hpp"
#include "spiderbot_defs.h"


void clock(){

  //set hour mode to 24
  RTC_MR &= ~(1 << 0);
  //stop the rtc
  RTC_CR |= (1 << UPDTIM);
  RTC_CR |= (1 << UPDCAL);
  //wait for the flag to go up
  while(!RTC_SR >> ACKUPD) & 1){}
  //clear flag
  RTC_SCCR |= (1 << ACKCLR);

  ///set any field/s in the clock registers
  RTC_TIMR = (12 << 16); //hours

  //set any field in the calendar registers
  //RTC_CALR =
  //start the rtc up again
  RTC_CR &= ~(1 << UPDTIM);
  RTC_CR &= ~(1 << UPDCAL);

}


/**
\defgroup Due bleToSerial
@{
*/

//global variables
int mode = 0; //holds current mode of bot (forward, bac, left etc)
boolean move_starting = false; //flag, a new move has started? if so, one-time actions might be needed


int interval = 500; //this should be synchronized to the
//servo move interval on the megas, possibly a bit longer

char serial_cmd_left[9];

leg legs[6];

long last_serial_command_sent =0;

long mode_set_time = 0;
long mode_max_time = 4000; //timeout of modes in 5 sec

#define STEP_FORWARD_TIME 0
#define STEP_BACKWARD_TIME 1
#define STEP_RIGHT_TIME 2
#define STEP_LEFT_TIME 3
//times the different moves take:
long move_times[8];

/**
set the time it should take to do a single action
*/
void setMoveExecTime(int interval){
Serial2.write('+');
Serial2.print(interval);

Serial3.write('+');
Serial3.print(interval);
}


/**
  gather the next moves from all the leg instances and send corresponding code via serials
*/
void sendCommands(){
  logln("sending commands :");
  char commandStrRight[10] = {0};
  commandStrRight[0] = '(';
  commandStrRight[1] = legs[R_FRONT_LEG].getTip();
  commandStrRight[2] = legs[R_FRONT_LEG].getMid();
  commandStrRight[3] = legs[R_FRONT_LEG].getBody();
  commandStrRight[4] = legs[R_MID_LEG].getTip();
  commandStrRight[5] = legs[R_MID_LEG].getMid();
  commandStrRight[6] = legs[R_MID_LEG].getBody();
  commandStrRight[7] = legs[R_BACK_LEG].getTip();
  commandStrRight[8] = legs[R_BACK_LEG].getMid();
  commandStrRight[9] = legs[R_BACK_LEG].getBody();

  char commandStrLeft[10] = {0};
  commandStrLeft[0] = '(';
  commandStrLeft[1] = legs[L_FRONT_LEG].getTip();
  commandStrLeft[2] = legs[L_FRONT_LEG].getMid();
  commandStrLeft[3] = 180-legs[L_FRONT_LEG].getBody();
  commandStrLeft[4] = legs[L_MID_LEG].getTip();
  commandStrLeft[5] = legs[L_MID_LEG].getMid();
  commandStrLeft[6] = 180-legs[L_MID_LEG].getBody();
  commandStrLeft[7] = legs[L_BACK_LEG].getTip();
  commandStrLeft[8] = legs[L_BACK_LEG].getMid();
  commandStrLeft[9] = 180-legs[L_BACK_LEG].getBody();
  //pump out serial commands
  log(commandStrRight);
  log( " : ");
  logln(commandStrLeft);
for(int i = 0; i<10; i++){
  Serial2.write(commandStrRight[i]);
  Serial3.write(commandStrLeft[i]);
}

}

/**
parse the serial-based commands into actions, set the mode variable
*/
void parseCmd(char cmd){
  //if a new move is requested, set move_starting flag

  if(mode != cmd){
    logln("requesting new mode");
    move_starting = true;
  }
  mode = cmd;
  mode_set_time = millis();
  switch(cmd){

    case CASE_FORWARD:{

      logln("take a step forward");
      break;
    }
    case CASE_BACKWARD:{

      logln("take a step backward");
      break;
    }
    case CASE_RIGHT:{

      logln("turn right");
      break;
    }
    case CASE_LEFT:{

      logln("turn left");
      break;
    }
    case CASE_STOPPED:{
      logln("stopping");
      break;
    }
    case  CASE_CALIB:{
      char calib_leg = ble_read()-48; //numbered legs
      char calib_servo = ble_read(); //TIP/MID/BODY
      char calib_pos = ble_read(); //UP/DOWN etc
      char calib_value = ble_read(); //new value of servo in pos pos
      log("calibrate servo ");
      log(calib_servo);
      log(" on leg ");
      log(calib_leg);
      log(" to ");
      log(calib_pos);
      log(" = ");
      log(calib_value);
      logln(" ");
      //legs[calib_leg].calibratePos(calib_pos, calib_servo, calib_value);
      break;
    }
    default:
    break;
  }
}


void setupLegs(leg * legs){
  legs[R_FRONT_LEG].setPins(R_FRONT_LEG, 13,12,11);
  legs[R_MID_LEG].setPins(R_MID_LEG, 10,9,8);
  legs[R_BACK_LEG].setPins(R_BACK_LEG, 7,6,5);
  legs[L_FRONT_LEG].setPins(L_FRONT_LEG, 13,12,11);
  legs[L_MID_LEG].setPins(L_MID_LEG,10,9,8);
  legs[L_BACK_LEG].setPins(L_BACK_LEG, 7,6,5);

  for(int i=0; i<6; i++){
    legs[i].calibratePos(UP, TIP, 70);
    legs[i].calibratePos(DOWN, TIP, 60);
    legs[i].calibratePos(UP, MID, 60);
    legs[i].calibratePos(DOWN, MID, 90);
    legs[i].calibratePos(FORTH, BODY, 60);
    legs[i].calibratePos(BACK, BODY, 120);

  }
}
/*
void walk(mode){

if(interval + last_serial_command_sent < millis())
  switch(mode){
    case CASE_FORWARD:
    case CASE_BACKWARD:
    case CASE_LEFT:
    case CASE_RIGHT:
    case CASE_STOPPED:
  }

}*/

/*
long loop_timer = 0;
long last_loop_end_millis = 0;
*/
void setup(){
setupLegs(legs);

    // Set your BLE Shield name here, max. length 10
    ble_set_name("Hamahakki");


    // Init. and start BLE library.
    ble_begin();

    // Enable serial debug
    Serial.begin(57600); // usb
    Serial1.begin(57600); // outgoing data //what is this?
    ///megas:
    Serial2.begin(57600); // vasen puoli
    Serial3.begin(57600); // oikea puoli
}

void walkStartPos(){
  legs[R_FRONT_LEG].setNext(UP);
  legs[R_MID_LEG].setNext(DOWN);
  legs[R_BACK_LEG].setNext(UP);

  legs[L_FRONT_LEG].setNext(DOWN);
  legs[L_MID_LEG].setNext(UP);
  legs[L_BACK_LEG].setNext(DOWN);
}

void loop(){

  //delay(1);
  //loop_timer = 0;

  if(ble_available()){

    char command = ble_read();
    log("received: ");
    logln(command);
    //set the movement mode
    parseCmd(command);

  }


  if((last_serial_command_sent + interval) <= millis()){
    //logln("updating");
    //regular actions

      switch(mode){
        case CASE_FORWARD:{
          //set walk pos
          if(move_starting){
            move_starting = false;
            walkStartPos();
            sendCommands();
          }else{
            cycleForward(legs,6);
            sendCommands();
          }

          break;
        }
        case CASE_BACKWARD:{
          if(move_starting){
            move_starting = false;
            walkStartPos();
            sendCommands();
          }else{
            cycleBackward(legs,6);
            sendCommands();
          }
          break;
        }
        case CASE_LEFT:{
          if(move_starting){
            move_starting = false;
            walkStartPos();
            sendCommands();
          }else{
            cycleTurnLeft(legs,6);
            sendCommands();
          }
          break;
        }
        case CASE_RIGHT:{
          if(move_starting){
            move_starting = false;
            walkStartPos();
            sendCommands();
          }else{
            cycleTurnRight(legs,6);
            sendCommands();
          }
          break;
        }
        case CASE_STOPPED:{
          stand(legs, 6);
          sendCommands();
          mode = 0;
          //mode_set_time-=mode_max_time; //enable next straight away
          break;
        }
      }

    last_serial_command_sent = millis();


    //timeout, bail out of stuff
    if(((mode_set_time + mode_max_time) < millis()) && mode != 0 ){
      stand(legs, 6);
      sendCommands();
      mode = 0;
    }
  }



    //if timing control says Mega should be done, send new command

    //process ble stuff
    ble_do_events();
    /*
    loop_timer = millis() - last_loop_end_millis;
    log("loop_timer: ");
    logln(loop_timer);
    last_loop_end_millis = millis();*/
}

/**@}*/

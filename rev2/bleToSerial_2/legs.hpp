#ifndef LEGS_HPP
#define LEGS_HPP

#include <Arduino.h>

template <typename T>
void log(T loggable);

template <typename T>
void logln(T loggable);

typedef class leg{
  int id;
//pin servo is attached to on Mega (config via serial)
  int tip_servo_pin;
  int mid_servo_pin;
  int body_servo_pin;
  //allow for calibrations
//mid and tip servo positions
  int up_tip;
  int up_mid;
  int down_tip;
  int down_mid;
//body servo positions
  int forth;
  int back;
  // home pos
  int home_tip = 70;
  int home_mid = 90;
  int home_body = 90;
  //hold next move to do, these two are needed to define walk cycles
  int next_move;
  int prev_move;

  int tip = 90;
  int mid = 90;
  int body = 90;

  int position_vertical;
  int position_horizontal;
  char side;
  boolean pending_changes = false;

public:
  leg();
  void setPins(int nid, int ntip_servo_pin,
  int nmid_servo_pin,
  int nbody_servo_pin);
  void calibratePos(int pos, int servo, int value);
  void setPos(int pos_hor, int pos_vert);

  int getPrev();
  void setPrev(int move);
  int getNext();
  void setNext(int move);
  int getTip();
  int getMid();
  int getBody();
  //set to home pos at 70/60/90 or something
  void setHome();


};

#endif

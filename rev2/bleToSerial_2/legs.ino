/**
\file legs.ino
*/

/**
wrapper for sending debug info to Serial0
*/
template <typename T>
void log(T loggable){
  Serial.print(loggable);
}

/**
wrapper for sending debug info to Serial0
*/
template <typename T>
void logln(T loggable){
  Serial.println(loggable);
}


leg::leg(){

}
void leg::setPins(int nid, int ntip_servo_pin,
int nmid_servo_pin,
int nbody_servo_pin){
  id = nid;
  tip_servo_pin = ntip_servo_pin;
  mid_servo_pin = nmid_servo_pin;
  body_servo_pin = nbody_servo_pin;
}

/**
predefine positions to take when calling setPos
eg. calibratePos(UP,TIP, 70 ); sets the "up" tip postion to serv setting 70
*/
void leg::calibratePos(int pos, int servo, int value){
  //constrain
  switch(servo){
    case TIP:
    if(value < 180 && value > 0){
      if(pos == UP){
        up_tip = value;
      }else if(pos == DOWN){
        down_tip = value;
      }
    }break;
    case MID:
    if(value < 180 && value > 0){
      if(pos == UP){
        up_mid = value;
      }else if(pos == DOWN){
        down_mid = value;
      }
    }break;
    case BODY:
    if(value < 180 && value > 0){
      if(pos == FORTH){
        forth = value;
      }else if(pos == BACK){
        back = value;
      }
    }break;
  }
}

/**
set the position to something, and change state to
indicate the serial digest which polls all the legs
*/
void leg::setPos(int pos_hor, int pos_vert){
  if(position_horizontal != pos_hor){
    position_horizontal = pos_hor;
    pending_changes = true;
  }
  if(position_vertical != pos_vert){
    position_vertical = pos_vert;
    pending_changes = true;
  }
}
/*
boolean leg::isPending(){
  return pending_changes;
}

void leg::resolvePending(){
  pending_changes = false;
}

int leg::getH(){
  return position_horizontal;
}
int leg::getV(){
  return position_vertical;
}
*/
int leg::getPrev(){
  return prev_move;
}
void leg::setPrev(int move){
  prev_move = move;
}
int leg::getNext(){
  return next_move;
}
void leg::setNext(int move){

  //prev_move = next_move;
  next_move = move;
    log("set nextpos on pin ");
    log(id);
  if(move == UP){
    log("to UP");
    tip = up_tip;
    mid = up_mid;
  }else if(move == DOWN){
    log("to DOWN");
    tip = down_tip;
    mid = down_mid;
  }
  if(move == FORTH){
    log("to FORTH");
    body = forth;
  }else if(move == BACK){
    log("to BACK");
    body = back;
  }
}

int leg::getTip(){
  return tip;
}
int leg::getMid(){
  return mid;
}
int leg::getBody(){
  return body;
}

void leg::setHome(){
  tip = home_tip;
  mid = home_mid;
  body = home_body;
}

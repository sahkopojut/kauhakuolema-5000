#ifndef SPIDERBOT_DEFS_H
#define SPIDERBOT_DEFS_H

/**
leg definitions
*/
#define R_FRONT_LEG 0
#define R_MID_LEG 1
#define R_BACK_LEG 2

#define L_FRONT_LEG 3
#define L_MID_LEG 4
#define L_BACK_LEG 5

/**
servo positions
*/
#define TIP 't'
#define MID 'm'
#define BODY 'b'
#define UP 'u'
#define DOWN 'd'
#define BACK 'b'
#define FORTH 'f'

#define CONTROL_SET_SERVO_DEBUG ')'
///(send single servo instructions [')',servonum(as cstring), value(as cstring)]
#define CONTROL_SET_SERVO '(' ///send full 9 positions as byte string
#define CONTROL_SET_INTERVAL '+' ///signal that following is parseable as an integer to set as the servo interval
#define CONTROL_STOP '%'
#define CONTROL_SET_ALL_90 '='

#define CASE_FORWARD 'w'
#define CASE_BACKWARD 's'
#define CASE_LEFT 'a'
#define CASE_RIGHT 'd'
#define CASE_STOPPED 'x'
#define CASE_CALIB '{'

#endif

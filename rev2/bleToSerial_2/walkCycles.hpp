/**
\file walkCycles.hpp
*/
#ifndef WALK_CYCLES_H
#define WALK_CYCLES_H

#include "legs.hpp"
/**
  set all legs in array to home position
  @param legs {leg*} the legs array to set
  @param leg_num {int} number of legs in array
*/
void stand(leg * legs, int leg_num);
/**
  mirrored left turn cycle
  @param legs {leg*} the legs array to set
  @param leg_num {int} number of legs in array
*/
void cycleTurnRight(leg * legs, int leg_num);
/**
  right turn cycle (in place turn, left goes forward and right backward)
  @param legs {leg*} the legs array to set
  @param leg_num {int} number of legs in array
*/
void cycleTurnLeft(leg * legs, int leg_num);
/**
  toggle all legs to get to the next position in walk backward cycle
  @param legs {leg*} the legs array to set
  @param leg_num {int} number of legs in array
*/
void cycleBackward(leg * legs, int leg_num);
/**
  toggle all legs to get to the next position in walk forward cycle
  @param legs {leg*} array of leg objects to set
  @param leg_num {int} number of legs to control
*/
void cycleForward(leg * legs, int leg_num);

#endif

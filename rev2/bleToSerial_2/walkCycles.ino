/**
\file walkCycles.ino
*/
#include "walkCycles.hpp"
/**
  toggle all legs to get to the next position in walk forward cycle
  @param legs {leg*} array of leg objects to set
  @param leg_num {int} number of legs to control
*/
void cycleForward(leg * legs, int leg_num){

  for(int i=0; i<6; i++){
    int prev = legs[i].getNext();
    if(prev == UP){
      legs[i].setNext(FORTH);
    }else
    if(prev == DOWN){
      legs[i].setNext(BACK);
    }else
    if(prev == FORTH){
      legs[i].setNext(DOWN);
    }else
    if(prev == BACK){
      legs[i].setNext(UP);
    }else{
      legs[i].setNext(DOWN);
    }
  }
}


/**
  toggle all legs to get to the next position in walk backward cycle
  @param legs {leg*} the legs array to set
  @param leg_num {int} number of legs in array
*/
void cycleBackward(leg * legs, int leg_num){

  for(int i=0; i<6; i++){
    int prev = legs[i].getNext();
    if(prev == UP){
      legs[i].setNext(BACK);
    }else
    if(prev == DOWN){
      legs[i].setNext(FORTH);
    }else
    if(prev == FORTH){
      legs[i].setNext(UP);
    }else
    if(prev == BACK){
      legs[i].setNext(DOWN);
    }else{
      legs[i].setNext(DOWN);
    }
  }
}
/**
  right turn cycle (in place turn, left goes forward and right backward)
  @param legs {leg*} the legs array to set
  @param leg_num {int} number of legs in array
*/
void cycleTurnLeft(leg * legs, int leg_num){
  //right side 0-2 has to go straight
  for(int i=0; i<3;i++){
    int prev = legs[i].getNext();
    if(prev == UP){
      legs[i].setNext(FORTH);
    }else
    if(prev == DOWN){
      legs[i].setNext(BACK);
    }else
    if(prev == FORTH){
      legs[i].setNext(DOWN);
    }else
    if(prev == BACK){
      legs[i].setNext(UP);
    }else{
      legs[i].setNext(DOWN);
    }
  }
  //left side has to walk backwards
  for(int i=3; i<6; i++){
    int prev = legs[i].getNext();
    if(prev == UP){
      legs[i].setNext(BACK);
    }else
    if(prev == DOWN){
      legs[i].setNext(FORTH);
    }else
    if(prev == FORTH){
      legs[i].setNext(UP);
    }else
    if(prev == BACK){
      legs[i].setNext(DOWN);
    }else{
      legs[i].setNext(DOWN);
    }
  }
}
/**
  mirrored left turn cycle
  @param legs {leg*} the legs array to set
  @param leg_num {int} number of legs in array
*/
void cycleTurnRight(leg * legs, int leg_num){
  //rigth side backwards
  for(int i=0; i<3;i++){
    int prev = legs[i].getNext();
    if(prev == UP){
      legs[i].setNext(BACK);
    }else
    if(prev == DOWN){
      legs[i].setNext(FORTH);
    }else
    if(prev == FORTH){
      legs[i].setNext(UP);
    }else
    if(prev == BACK){
      legs[i].setNext(DOWN);
    }else{
      legs[i].setNext(DOWN);
    }
  }
//left side forward
  for(int i=3; i<6;i++){
    int prev = legs[i].getNext();
    if(prev == UP){
      legs[i].setNext(FORTH);
    }else
    if(prev == DOWN){
      legs[i].setNext(BACK);
    }else
    if(prev == FORTH){
      legs[i].setNext(DOWN);
    }else
    if(prev == BACK){
      legs[i].setNext(UP);
    }else{
      legs[i].setNext(DOWN);
    }
  }
}

/**
  set all legs in array to home position
  @param legs {leg*} the legs array to set
  @param leg_num {int} number of legs in array
*/
void stand(leg * legs, int leg_num){
  for(int i=0; i<leg_num;i++){
    legs[i].setHome();
  }
}

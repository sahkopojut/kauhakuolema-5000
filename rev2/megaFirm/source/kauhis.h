#ifndef KAUHIS_H
#define KAUHIS_H


#define FORWARD 1
#define BACKWARD 2
#define UP 3
#define DOWN 4


#define SERVO_BODY 3
#define SERVO_MID 2
#define SERVO_TIP 1

#define LEG_FRONT 1
#define LEG_MID 2
#define LEG_BACK 3


///control char for setting a single servo
#define CONTROL_SET_SERVO_DEBUG ')'
///control char for setting all the servos to the next 9 values received
#define CONTROL_SET_SERVO '('
///control char for setting the interval to the next value of Serial1.parseInt()
#define CONTROL_SET_INTERVAL '+'
///control char for stopping all movement immediately
#define CONTROL_STOP '%'
///control char for setting all servos to 90 immediately
#define CONTROL_SET_ALL_90 '='

#endif

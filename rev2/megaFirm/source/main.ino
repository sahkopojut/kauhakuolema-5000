/**@file main.ino */
#include <Arduino.h>
#include <Servo.h>
#include "kauhis.h"

/**
\defgroup Mega serialReceiver
@{
*/

///interval from prev servo pos to next servo pos (time between starting one command and the next)
int interval = 500;

///Portpins the servos are attached to
char servoPorts[9] = {13,12,11,10,9,8,7,6,5};

  ///start millis() of the current move
  long start_move = 0;

///holds current values of servos
int servo_values[9] = {90};
///holds next values (obtained via serial)
int next_servo_values[9] = {90};
///buffer for servo values that come during  digest interval
int buffered_servo_values[9] ={90};
///start positions of servos (this held before a move comes down the pipes)
int prev_servo_values[9] = {90};
///flag to enable/disable logging to USB-serial
boolean logging = true;

///keeps track of state, are we moving or not? Enables/disables digest function
boolean moving = false;

///digest needed flag, set when a new command is received and cleared when digest is enabled
boolean digest_flush = false;

///millis() of the last serial reception, used to allow full commands to accumulate to the serial buffer
long lastSerialReceive = 0;
///interval for polling the serial buffer, @see lastSerialReceive
int serialReceiveInterval = 50;
///servo objects (Arduino standard Servo.h library)
Servo servos[9];


/**
  wrapper function for logging/debugging to the USB serial port, wraps Serial.print(T)
*/
template <typename T>
void log(T loggable){
  Serial.print(loggable);
}

/**
  wrapper function for logging/debugging to the USB serial port, wraps Serial.println(T)
*/
template <typename T>
void logln(T loggable){
  Serial.println(loggable);
}

/**
attach servos to ports, set the initial values to parameterized value
*/
void initServos(int value){

  for(int i=0; i<9; i++){
    servos[i].attach(servoPorts[i]);
    servo_values[i] = value;
    next_servo_values[i] = value;
    buffered_servo_values[i] = value;
    prev_servo_values[i] = value;
  }

}


/**
check if servos need moving, move them and keep track of state
@param moving {bool} should we try to move?
@param interval {int} milliseconds the move from prev_servo_values to next_servo_values should take
@param start_move {long} the start of the current move cycle, used to calculate intermittent servo values
@param prev_servo_values {*int} the array of servo values from before the current move
@param next_servo_values {*int} array of servo values that are to be reached by interval+start_move
@param servo_values {*int} array of current servo values, the value in here is written to servos every digest
@returns moving_result {bool} true if the move is not finished, false if all servos are at next_servo_values.
*/
bool digestServos(bool moving, int interval, long start_move, int *prev_servo_values, int *next_servo_values, int*servo_values){
  int allDone = 0;
  if(moving){

    int now = start_move + interval - millis();

    for(int i=0; i<9; i++){

      if(servo_values[i] != next_servo_values[i]){

        servo_values[i] = map(now, interval, 0, prev_servo_values[i], next_servo_values[i] );

        servos[i].write(servo_values[i]);
      }

      //check if all are done
      if(servo_values[i] == next_servo_values[i]){
        //moving = false; //end value reached
        allDone++;
      }
    }
    //every field in servo_values is set to next_servo_values
    if(allDone >= 9){
      moving = false;
      return false;
    }else{
      return true;
    }
  }

}


/**
 sets servo at pos servo to target value, trigger next digest cycle

@param servo {char} servo to set next digest cycle
@param value {int} value to set servo to
*/
bool setServo(char servo, int value){
  //put the newest commanded value  into buffer
  if(buffered_servo_values[servo] != value){

    buffered_servo_values[servo] = value;

    //notify digest logic about a new servo value (digest needed)
    digest_flush = true;

    return true;
  }else return false; //no move needed this time
}

/**
 stops all movement asap, all servo_values are written to the input buffers
*/
void stopItNow(){
  for(int i=0; i<9; i++){
    //Serial.println("stopping!");
    logln("stopping");
    buffered_servo_values[i] = servo_values[i];
    next_servo_values[i] = servo_values[i];
  }
}

/**
   reads the next command from serial, if the next byte is not a control
  symbol, read serial to eventually flush out the garbage message, otherwise enacts the command
*/
void parseSerialCmd(){


  if((lastSerialReceive + serialReceiveInterval) < millis()){

      lastSerialReceive = millis();
      if(Serial1.available()){
        int serial_num = Serial1.available();
        //check for control char (set all servo values),
        //if rxed a value outside of 0-180, do nothing (old value persists)
        if(serial_num > 1 && Serial1.peek() == CONTROL_SET_SERVO){
          Serial1.read(); //get rid of control char
            //get all nine servo values (most dense way to convey information)
            for(int i=0; i<9; i++){
              int rxed = Serial1.read();
              if(rxed > 0 && rxed < 180){
                //que up servo vvalues into buffer
                setServo(i, rxed);

              }
            }
        }else if(Serial1.peek() == CONTROL_SET_INTERVAL){

          Serial1.read();
          //here come problems later
          interval = Serial1.parseInt();

        }else if(Serial1.peek() == CONTROL_STOP){
          Serial1.read();
          stopItNow();

        }else if(Serial1.peek() == CONTROL_SET_SERVO_DEBUG){
          //set single servo in format {control, servonum, value}
          //plaintext input, alter if this function becomes needed with due
          Serial1.read();
          int servonum = Serial1.parseInt();
          int value = Serial1.parseInt();

          log("[debugging] set servo ");
          log(servonum);
          log("to value ");
          logln(value);

          setServo(servonum, value);

        }else if(Serial1.peek() == CONTROL_SET_ALL_90){
          Serial1.read();
          for(int i=0; i<9; i++){
            setServo(i, 90);
          }

        }
        else{
          //garbled char probably
          Serial1.read();
        }

      }
  }

}

/**
check if digest is triggered
*/
void checkForFlush(){
  //digest flush triggers this block, setting the next_servo_values and starting the timer
  if(digest_flush){

      //if last move is finished, start next
    if(!moving){

      logln("start move : ");

      for(int i=0; i<9; i++){
        //move the buffered values to be executed in next digest cycle
        next_servo_values[i] = buffered_servo_values[i];
        prev_servo_values[i] = servo_values[i];

        log("servo ");
        log(i);
        log(" : ");
        log(prev_servo_values[i]);
        log(" to ");
        logln(next_servo_values[i]);


      }
      //put up start time of move for the mapping in digest
      start_move = millis();
      //set the "moving" flag (this is only called again once digest zeroes out "moving")
      moving = true;
      //stop calling this function until digest finishes setting the servos (over interval)
      digest_flush = false;

    }
  }
}

/**
 using the standard Arduino structure, initializes serial comms and servos
*/
void setup(){

Serial.begin(57600);
Serial1.begin(57600);
initServos(90);
}
/**
 main loop, Arduino structure since why not
*/
void loop(){

parseSerialCmd();
checkForFlush();

//run digest until it signals that all moves are done by returning false
moving = digestServos(  moving,
                        interval,
                        start_move,
                        prev_servo_values,
                        next_servo_values,
                        servo_values);
}

/**
@}
*/
